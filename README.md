# Test Autobonplan



## Ta mission, si tu l'acceptes

Nous souhaitons avoir une visibilité sur l'évolution du volume d'appels reçus en concession.

Exemple: Voir rapidement que Renault Saint Herblain a reçu plus d'appels que la semaine dernière.

Pour t'aider, tu trouveras ci-joint :

 - une maquette pour te guider dans la réalisation graphique
 - un fichier.csv contenant un échantillon de 500 appels du mois de mai
 - les premières briques du projet

## Contraintes

- [ ] Donner la possibilité d'uploader un nouveau fichier d'appel
- [ ] Créer un système d'authentification avec 2 users (1 lecture/import et 1 lecture)
- [ ] Créer une base de données relationnelle
- [ ] Utiliser Php (POO sans framework) et Javascript 
- [ ] Gestion de projets via Git

## Bonus

- [ ] Solution responsive
- [ ] Si tu en as la possibilité, il faudrait que tu puisses déployer ton application en ligne pour que nous puissions l'étudier.

## Contenu de la page

- TOTAL APPELS REÇUS : total des appels de la semaine
- TOTAL PRISE EN CHARGE : total des appels répondu (= appel avec une durée)
- Graphique 1 (colonne) : total des appels/concession de la semaine et total des appels/concession de la semaine dernière
- Graphique 2 (ligne) : total des appels répondu/concession de la semaine et total des appels répondu/concession de la semaine dernière

